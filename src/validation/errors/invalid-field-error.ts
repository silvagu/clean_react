export class InvalidFieldError extends Error {
  constructor() {
    super('The field is invalid')
    this.name = 'InvalidFieldError'
  }
}
