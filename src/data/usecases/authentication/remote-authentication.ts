import { HttpPostClient } from '@/data/protocols/http/http-post-client'
import {
  Authentication,
  AuthenticationParams,
} from '@/domain/usecases/authentication'
import {
  NotFoundError,
  UnexpectedError,
  InvalidCredentialsError,
} from '@/domain/errors'
import { HttpStatusCode } from '@/data/protocols/http/http-response'
import { AccountModel } from '@/domain/models'

export class RemoteAuthentication implements Authentication {
  constructor(
    private readonly url: string,
    private readonly httpPostClient: HttpPostClient<
      AuthenticationParams,
      AccountModel
    >
  ) {}

  async auth(params: AuthenticationParams): Promise<AccountModel> {
    const httpResponse = await this.httpPostClient.post({
      url: this.url,
      body: params,
    })
    switch (httpResponse.statusCode) {
      case HttpStatusCode.ok:
        return httpResponse.body
      case HttpStatusCode.unauthorized:
        throw new InvalidCredentialsError()
      case HttpStatusCode.notFound:
        throw new NotFoundError()
      default:
        throw new UnexpectedError()
    }
  }
}
