import { AuthenticationParams } from '@/domain/usecases'
import { AccountModel } from '@/domain/models'
import faker from 'faker'

export const mockAuthentication = (): AuthenticationParams => {
  const password = faker.internet.password()
  return {
    name: faker.name.title(),
    email: faker.internet.email(),
    password: password,
    passwordConfirmation: password,
  }
}

export const mockAccountModel = (): AccountModel => ({
  accessToken: faker.datatype.uuid(),
  name: faker.name.title(),
})
