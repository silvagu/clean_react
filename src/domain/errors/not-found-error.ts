export class NotFoundError extends Error {
  constructor() {
    super('Page not found')
    this.name = 'NotFoundError'
  }
}
